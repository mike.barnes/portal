from typing import Optional, List

from tldextract import extract

from app.brm.brn import BRN
from app.extensions import db
from app.models import AbstractConfiguration, AbstractResource
from app.models.onions import Onion


class Origin(AbstractConfiguration):
    group_id = db.Column(db.Integer, db.ForeignKey("group.id"), nullable=False)
    domain_name = db.Column(db.String(255), unique=True, nullable=False)
    auto_rotation = db.Column(db.Boolean, nullable=False)
    smart = db.Column(db.Boolean(), nullable=False)
    assets = db.Column(db.Boolean(), nullable=False)

    group = db.relationship("Group", back_populates="origins")
    proxies = db.relationship("Proxy", back_populates="origin")

    @property
    def brn(self) -> BRN:
        return BRN(
            group_id=self.group_id,
            product="mirror",
            provider="conf",
            resource_type="origin",
            resource_id="self.domain_name"
        )

    @classmethod
    def csv_header(cls) -> List[str]:
        return super().csv_header() + [
            "group_id", "domain_name", "auto_rotation", "smart"
        ]

    def destroy(self) -> None:
        super().destroy()
        for proxy in self.proxies:
            proxy.destroy()

    def onion(self) -> Optional[str]:
        tld = extract(self.domain_name).registered_domain
        onion = Onion.query.filter(Onion.domain_name == tld).first()
        if not onion:
            return None
        domain_name: str = self.domain_name
        return f"https://{domain_name.replace(tld, onion.onion_name)}.onion"


class Proxy(AbstractResource):
    origin_id = db.Column(db.Integer, db.ForeignKey("origin.id"), nullable=False)
    pool_id = db.Column(db.Integer, db.ForeignKey("pool.id"))
    provider = db.Column(db.String(20), nullable=False)
    psg = db.Column(db.Integer, nullable=True)
    slug = db.Column(db.String(20), nullable=True)
    terraform_updated = db.Column(db.DateTime(), nullable=True)
    url = db.Column(db.String(255), nullable=True)

    origin = db.relationship("Origin", back_populates="proxies")
    pool = db.relationship("Pool", back_populates="proxies")

    @property
    def brn(self) -> BRN:
        return BRN(
            group_id=self.origin.group_id,
            product="mirror",
            provider=self.provider,
            resource_type="proxy",
            resource_id=str(self.id)
        )

    @classmethod
    def csv_header(cls) -> List[str]:
        return super().csv_header() + [
            "origin_id", "provider", "psg", "slug", "terraform_updated", "url"
        ]


class SmartProxy(AbstractResource):
    group_id = db.Column(db.Integer(), db.ForeignKey("group.id"), nullable=False)
    instance_id = db.Column(db.String(100), nullable=True)
    provider = db.Column(db.String(20), nullable=False)
    region = db.Column(db.String(20), nullable=False)

    group = db.relationship("Group", back_populates="smart_proxies")

    @property
    def brn(self) -> BRN:
        return BRN(
            group_id=self.group_id,
            product="mirror",
            provider=self.provider,
            resource_type="smart_proxy",
            resource_id=str(1)
        )
