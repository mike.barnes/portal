from io import BytesIO
from typing import BinaryIO

import openpyxl
import requests

from app.terraform.block.bridge import BlockBridgeAutomation

DOWNLOAD_URL = "https://dnsc.ro/vezi/document/situatie-site-uri-cu-activitate-in-contextul-crizei-ucraina-rusia-plus" \
               "-adrese-ip-specifice-utilizate-in-atacuri-malware-detalii"


class BlockBridgeDnscAutomation(BlockBridgeAutomation):
    short_name = "block_bridge_dnsc"
    description = "Import the Romanian National Directorate for Cybersecurity blocklist"
    frequency = 60 * 24

    _content: BinaryIO

    def fetch(self) -> None:
        response = requests.get(DOWNLOAD_URL,
                                headers={"User-Agent": "Mozilla/5.0 (Windows NT x.y; Win64; x64; rv:10.0) "
                                                       "Gecko/20100101 Firefox/10.0"})
        self._content = BytesIO(response.content)

    def parse(self) -> None:
        wb = openpyxl.open(self._content)
        sheet = wb[wb.sheetnames[0]]
        idx = 2
        while True:
            addr = sheet['B' + str(idx)].value
            if not addr:
                break
            self.ips.append(addr)
            idx += 1
