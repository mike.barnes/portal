from typing import List, Dict, Union, Optional

from pydantic import BaseModel

from app.models.base import Pool
from app.models.mirrors import Proxy


class RedirectorPool(BaseModel):
    short_name: str
    description: str
    api_key: str
    redirector_domain: Optional[str]
    origins: Dict[str, str]


class RedirectorData(BaseModel):
    version: str
    pools: List[RedirectorPool]


def redirector_pool_origins(pool: Pool) -> Dict[str, str]:
    origins: Dict[str, str] = dict()
    active_proxies = Proxy.query.filter(
        Proxy.deprecated.is_(None),
        Proxy.destroyed.is_(None),
        Proxy.url.is_not(None),
        Proxy.pool_id == pool.id
    )
    for proxy in active_proxies:
        origins[proxy.origin.domain_name] = proxy.url
    return origins


def redirector_pool(pool: Pool) -> RedirectorPool:
    return RedirectorPool(
        short_name=pool.pool_name,
        description=pool.description,
        api_key=pool.api_key,
        redirector_domain=pool.redirector_domain,
        origins=redirector_pool_origins(pool)
    )


def redirector_data(_: Optional[Pool]) -> Dict[str, Union[str, Dict[str, Union[Dict[str, str]]]]]:
    active_pools = Pool.query.filter(
        Pool.destroyed.is_(None)
    ).all()
    return RedirectorData(
        version="1.0",
        pools=[
            redirector_pool(pool) for pool in active_pools
        ]
    ).dict()
